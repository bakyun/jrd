package jrd

// JRD is a JSON Resource Descriptor, specifying properties and related links
// for a resource.
type JRD struct {
	Subject    string                 `json:"subject,omitempty"`
	Aliases    []string               `json:"aliases,omitempty"`
	Properties map[string]interface{} `json:"properties,omitempty"`
	Links      []Link                 `json:"links,omitempty"`
}

// New JRD object
func New() *JRD {
	jrd := new(JRD)
	jrd.Aliases = make([]string, 0)
	jrd.Properties = make(map[string]interface{})
	jrd.Links = make([]Link, 0)

	return jrd
}

// AddLink to the JRD object
func (jrd *JRD) AddLink(link Link) {
	jrd.Links = append(jrd.Links, link)
}

// GetLinkByRel returns the first *Link with the specified rel value.
func (jrd *JRD) GetLinkByRel(rel string) *Link {
	for _, link := range jrd.Links {
		if link.Rel == rel {
			return &link
		}
	}
	return nil
}

// GetAllLinksByRel returns all *Link with the specified rel value.
func (jrd *JRD) GetAllLinksByRel(rel string) []*Link {
	var links []*Link
	for _, link := range jrd.Links {
		if link.Rel == rel {
			links = append(links, &link)
		}
	}
	return links
}

// FilterLinksByRels modifies the JRD to only include the links with the specified rels
func (jrd *JRD) FilterLinksByRels(rel ...string) {
outerLoop:
	for i := len(jrd.Links) - 1; i >= 0; i-- {
		link := jrd.Links[i]

		for _, r := range rel {
			if r == link.Rel {
				jrd.Links = append(jrd.Links[:i], jrd.Links[i+1:]...)
				continue outerLoop
			}
		}
	}
}

// GetProperty Returns the property value as a string.
// Per spec a property value can be null, empty string is returned in this case.
func (jrd *JRD) GetProperty(uri string) string {
	if jrd.Properties[uri] == nil {
		return ""
	}
	return jrd.Properties[uri].(string)
}
