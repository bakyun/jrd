package jrd

// Link describes a single link in a full JRD
type Link struct {
	Rel        string                 `json:"rel"`
	Type       string                 `json:"type,omitempty"`
	Href       string                 `json:"href,omitempty"`
	Template   string                 `json:"template,omitempty"`
	Titles     map[string]string      `json:"titles,omitempty"`
	Properties map[string]interface{} `json:"properties,omitempty"`
}

// NewLink creates a new Link entry
func NewLink(rel string) Link {
	return Link{Rel: rel}
}

// GetProperty Returns the property value as a string.
// Per spec a property value can be null, empty string is returned in this case.
func (link *Link) GetProperty(uri string) string {
	if link.Properties[uri] == nil {
		return ""
	}
	return link.Properties[uri].(string)
}
